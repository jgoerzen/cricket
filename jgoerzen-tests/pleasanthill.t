!../Build/output.z8

Pleasant Hill
so thick

; Make sure we get the error message for directions.
> n
much too thick
> s
much too thick
> e
much too thick
> w
much too thick

; Make sure we get the message for going up and down.

> up
As you climb the stalk
Pleasant Hill Stalktop

> down
At first, you try to slide

> climb stalks
As you climb the stalk

> climb stalks
At first, you try to slide

; Examine some stuff

> x cricket
quite grumpy

> x stalks
nice shade of bright green

> take stalks
hardly portable

> take cricket
would care for that

> hint
I wonder if you are a good climber

> listen
You hear the giant stalks blowing

> listen to cricket
beginning to suspect

> smell
antennae-waving



> quit

