"The Grumpy Cricket (And Other Enormous Creatures)" by "John Goerzen"

[ Copyright (c) 2023 John Goerzen ]

Use American dialect.

Use the serial comma.

[ Include Basic Help Menu by Emily Short. ]

Chapter - Game-wide Settings and Intro

The story headline is "A game for the mischievous".

The release number is 1.

The story genre is "comedy".

The story creation year is 2023.

Release along with an interpreter.

After printing the banner text, say "[line break]Copyright (c) 2023 John Goerzen <jgoerzen@complete.org>.  This program comes with absolutely no warranty and is distributed under the terms of the GNU General Public License version 3.  See https://www.complete.org/the-grumpy-cricket for details.

 ~ For Jacob, Oliver, and Martha with love from Dad ~

What a happy day!  You've just finished an exciting visit with your cousins, and now it's time to head home.  It's too far to walk, so you have set out in the world to find a way home.  And, you hope, have some fun along the way.

You can type HELP to get some generic ideas, or HINT to get some ideas at any time.[line break]".

Instead of tasting, say "Although you are rather proud of your muscular jaws, you decide that might not be a great idea."

Instead of smelling, say "You wave your antennae about expectantly, but you smell nothing surprising.

Still, it's always fun to go antennae-waving!"

Understand "hint" as hinting.

Understand "help" as helping.

Helping is an action applying to nothing.

Hinting is an action applying to nothing.

Carry out helping:
	say "You are in charge!  Tell me what to do.  Give me simple commands that start with verbs.  For instance, LOOK tells me to look around.  HINT has me give you a hint.
	
	Most commands will also act in a direction or on something.  For instance, GO NORTH to go, well, north.  Or GO UP and so forth.  Some things you can pick up; for instance, TAKE SHOVEL has me pick up... yes, a shovel.
	
	A very useful command is EXAMINE.  It means take a closer look at something.  You can EXAMINE the stalks, signs, and so forth.
	
	SAVE your progress periodically."

Chapter - The Happy Day

Section 1 - Pleasant Hill

Pleasant Hill is a room.  "It's beautiful, sunny, and warm today.  But where you are standing, it's pleasantly cool and shady; enormous green stalks, gently waving in the wind, provide plenty of shade.  Your legs feel warm from the breeze blowing past them, but your bare feet feel pleasantly cool from the soft earth beneath you on a little hill.

It all combines to make you feel quite pleasant.  So pleasant, in fact, that you could just lay down and take a nice nap.  [if cricket is unhappy]Or, rather, you would, if it weren't for that huge, and rather grumpy, cricket.[else if cricket is bandaged]Or, rather, you would, if it weren't for that rather angry cricket glaring at you from beneath his fresh bandage.[else]But you don't think you'd fall asleep with the cricket chirping so happily nearby.[end if]

The green stalks are so thick that you can't see far enough to make your way down here.  Good thing you're a fantastic climber; you'll probably have more to see up high.

There is a small path to the north[if cricket is unhappy], but the cricket is standing in the middle of it.[else if cricket is bandaged], but the acorn is blocking the way.[else].[end if]"

In the Pleasant Hill is a cricket.  The cricket is a male animal.  The cricket can be unhappy, bandaged, or sitting.  The cricket is unhappy.

The description of the cricket is "To your eyes, the cricket is huge - much larger than you. [if the cricket is unhappy]He looks old, tired, bored, and, well, quite grumpy. He seems to want to be left alone.  He notices you looking at him, and sadly says 'Chiiioooooiiirp', long and slow and loud.  You can't be sure, but suspect he's saying 'leave me alone.'  You decide that you will do just that, because he looks boring.  Well, also because he's the size of a basketball court and rather grumpy.  As you back away, you tell yourself you're not scared of the enormous cricket.  Not at all. Nope.[else if the cricket is bandaged]His head has a bandage on it.  He looks old, angry, sorta headache-ish, and maybe still grumpy too.  Now he REALLY wants to be left alone.[else]He is sitting on the acorn.  You're not sure if crickets can smile, but this one seems to be.  He chirps gently and happily at you.[end if]"

In the Pleasant Hill are enormous green stalks.  The stalks are scenery. The description of the stalks is "The stalks are a nice shade of bright green.  They're tall; much taller than you.  They remind you of pictures you've seen of trees in a rainforest: big, and so thick they make it shady down below.  The stalk feels rough and textured to your touch; it should be easy to climb."

Before listing nondescript items:
    Now the enormous green stalks are not marked for listing; now the cricket is not marked for listing.

Instead of going nowhere in Pleasant Hill, say "The stalk-jungle is much too thick down here to move very far at all.  You're going to have to find a way to get up higher before you go anywhere.  Perhaps there's something tall you could climb?"

Instead of going north in Pleasant Hill when the cricket is unhappy, say "You take a step or three in that direction, telling yourself you are not scared of the cricket.  The cricket watches you and seems to be.. growling?  Can crickets growl?  You're not sure, but you are pretty sure this one isn't happy to have you come closer.

You're definitely not scared of the cricket.  You simply decide it wouldn't be polite to go near, so you stay where you are."

Instead of going north in Pleasant Hill when the cricket is bandaged, say "There's a large acorn blocking the path north.  You wonder if it could be pushed aside."

Instead of listening, say "You hear the giant stalks blowing in a pleasant breeze.  [if the cricket is unhappy]Somewhat less pleasantly, you hear a cricket nearby.  You don't speak Cricketish, but you strongly suspect the cricket is making sounds that are, well, grumpy.[end if]".

Instead of listening to the cricket, say "[if the cricket is sitting]You still don't speak Cricketish, but you think the cricket might be making content sounds.[else]You don't speak Cricketish, but you are beginning to suspect that the cricket's sounds are... well.... grumpy.[end if]".

Waving to is an action applying to one visible thing.  Understand "wave to [someone]" as waving to.

Carry out waving to:
	say "You wave several of your legs in a friendly fashion, but the [noun] doesn't react."
	
Instead of waving to the cricket, say "[if the cricket is sitting]The cricket raises a leg and waves back happily![else]The cricket doesn't seem to notice you waving.  Or maybe it is just too grumpy to care.[end if]"


[ This overrides "say" also. See https://intfiction.org/t/how-do-people-talk-to-each-other-in-inform/563 ]
Instead of answering the cricket that, say "You don't have the kind of legs that can chirp in Cricketish."

Talking to is an action applying to one visible thing.  Understand "talk to [someone]" or “converse with [someone]” as talking to.

Carry out talking to:
	say "The [noun] seems to be bored and unaware of your witty conversation."

Instead of talking to the cricket, say "You start jabbering away happily, telling the cricket about your day with your cousins and your journey home.  The cricket looks at you with confusion.  Then you realize that you don't have the kind of legs that can chirp in Cricketish, and the cricket has no idea what you're saying."

Understand "stalk" as the enormous green stalks.

Instead of climbing the enormous green stalks:
	try going up.

Carry out hinting in Pleasant Hill:
	say "[if the cricket is unhappy]I wonder if you are a good climber?  Those stalks look enticing.  Try 'GO UP' or 'CLIMB STALKS'.[else if the cricket is bandaged]Maybe pushing the acorn would clear the path?[else]Looks like the path north is finally open![end if]"

Instead of pushing the acorn in Pleasant Hill when the cricket is bandaged:
	now the cricket is sitting;
	now the cricket is on the acorn;
	say "You walk up to the acorn and push with all your might.  The acorn starts rolling away from the path and...
	
Oh no.  Not again.  It's rolling right for the cricket!  Just when it almost hits him, he jumps up.  He lands on the acorn and... laughs?  Yes, he seems to be chirping with laughter.  He sits down and waves a leg at you happily.  He sings you a nice chirpy song, then leans back and relaxes.

Could it be that what the cricket wanted all this time was... a chair?  And the acorn is a comfy chair for a cricket?"

Instead of pushing the acorn in Pleasant Hill when the cricket is sitting:
	say "Although the cricket looks pretty friendly, he's still a lot bigger than you, and might not like it if you roll away his chair.  You decide not to touch it."

Instead of pulling the acorn in Pleasant Hill:
	say "That would be dangerous.  It's a lot bigger than you and you don't want to get squished under it!"

Above Pleasant Hill is Pleasant Hill Stalktop.

Before going from Pleasant Hill to Pleasant Hill Stalktop, say "As you climb the stalk, you feel it gently waving back and forth in the breeze.  At the top, it reminds you of an upside-down swing: the bottom part is still, but by holding on and swinging back and forth, you can make it swing all over from up here.  You do this for a few minutes, yelling 'Wheee!' while you feel the wind rush all around you at the top of the stalk-swing.

[if the cricket is unhappy]Eventually, a distant cricket seems to be annoyed by all your carrying on, and makes a rather grumpy-sounding chirp.   You decide it might be best not to further annoy the already-grumpy cricket -- though part of you wants to see what would happen if you did.  You decide to calm down a bit.[else]Your legs eventually get tired, so you relax a bit.[end if]"

Before going from Pleasant Hill Stalktop to Pleasant Hill, say "At first, you try to slide down the stalk like a fire-station pole, but it's too rough for that, so you slow down and do it properly.[if the cricket is bandaged]

As you go down, you notice the cricket.  Now he's not just grumpy; he looks positively ANGRY.  Also he has a large bandage on his head.  He gives you a loud and long 'CHIRP!'  You decide it would be wise to stay away from him."

Section 2 - Pleasant Hill Stalktop

Pleasant Hill Stalktop is a room.  "As you slowly sway back and forth at the top of an enormous stalk, you look around.  To the south, you see a shallow stream, slowly burbling as it flows by to the west.  Off in the distance to the north, there's a tall building of some sort, but you can't see it very well from here and can't tell if there's anything between you and it.  To the east, there is a quite tall tree.

From here, you see all sorts of activity below; two young grasshoppers are playing tag, now and then jumping high above the stalks where you can see them, then disappearing back down again.  You're smaller than all of the creatures, except the ants, which are about your size."
	
[
A stream is here.  "To the south, you see a shallow stream, slow burbling as it flows by to the west."  The stream is fixed in place.] [  The description of the stream is "The stream seems peaceful, but there are lots of these tall stalks between here and there, so you can't see it very well." ]
	
A stream is scenery in Pleasant Hill Stalktop.  The description of the stream is "The stream seems peaceful, but there are lots of these tall stalks between here and there, so you can't see it very well."

A tree is scenery in Pleasant Hill Stalktop.  The description of the tree is "Well, it's a tree.  You know the type: brownish, with green leaves.  Pretty tall.  Doesn't say much, even if you talk to it."

A building is scenery in Pleasant Hill Stalktop.  The description of the building is "It seems pretty solid.  Pretty big.  You're not very close, so it's hard to make out much detail."

In the Pleasant Hill Stalktop are enormous green stalk tops.  The description of enormous green stalk tops is "From way up here, you can only see the tops of the green stalks; some of them end in a brownish jagged top."

Young grasshoppers is scenery in Pleasant Hill Stalktop.  "They seem to have a lot of energy.  They are -- prepare to be surprised -- hopping."

Ants is scenery in Pleasant Hill Stalktop.  "They are running around looking busy.  Except for the one that's just sitting there taking a nap."

Understand "stalk" as the enormous green stalk tops.
Understand "stalks" as the enormous green stalk tops.
Understand "tops" as the enormous green stalk tops.

Instead of climbing the enormous green stalk tops:
	try going down.
	
Before listing nondescript items:
    Now the enormous green stalk tops are not marked for listing.

Instead of going south in Pleasant Hill Stalktop, say "You work your legs and make the stalks wave, then jump from stalk to stalk like a monkey.  This is grea...  YIKES!  Frogs!  There are frogs by the stream!  They are much bigger than you, and they are watching you hungrily.

You decide to go back the way you came, and quickly!  The frogs watch you go for a minute, then go back to sitting around and... being hungry, I guess."

Instead of going west in Pleasant Hill Stalktop:
	try going south.
	
Before going east in Pleasant Hill Stalktop, say "You work your legs and make the stalks wave, then jump from stalk to stalk like a monkey.  This is great!"

Carry out hinting in Pleasant Hill Stalktop:
	say "[if cricket is unhappy]You might try to GO EAST and check out the tree.[else]Perhaps it's time to GO DOWN and see what's going on with the cricket.[end if]"

Section - Tree Base

Tree Base is a room.  "You are at the base of a tall tree.  It's so tall you can barely see the top, even if you tilt your head all the way back.

To the west, you see the stalktops waving in the breeze."

Tree Base is east of Pleasant Hill Stalktop.

Tall Tree is a backdrop.  It is in Base and Treetop.  "It's a tree all right.  You know the type: brownish, with green leaves.  Pretty tall.  Pretty thick.  Doesn't say much, even if you talk to it.

Looks like it's been here a good long while.  You couldn't even come close to wrapping your legs around it.  The tree's rough brown bark provides convenient footholds - some of them large enough for you to climb in and take a nap."

Carry out hinting in Tree Base:
	say "[if cricket is unhappy]You should probably EXAMINE TREE if you haven't already.  It might give you an idea about something you should do vertically.[else]You'd better go west to check on that cricket.[end if]"
	
Instead of climbing Tall Tree in Tree Base:
	try going up.

	
Section - Treetop

Treetop is a room. "You're standing on a nice green leaf near the top of the tree.  You ponder taking a nap; the leaf is like a big hammock, and could easily hold you and ten of your friends.

You can also walk around on the leaf, and you are careful to hold on when you do, because it is blowing every which way in the stiff wind up here.

You are so high up you can't really make out the details on the ground.  You can't see the grasshoppers, and even the cricket is too small to see.

To the north, you see a spot of gray in front of a large building.  Past the building, there are some large yellow things with wheels.  That's how you got here, so you think maybe one of them could take you home.

You shift your gaze to the south and west, and see the froggy stream below.  You're so high up you can see right past it, and you see houses in the distance."

Treetop is above Tree Base.

Before going from Tree Base to Treetop, say "You climb the tree, higher and higher.  This is exhausting!  You pause periodically to rest.

Finally you reach the top and climb out on a nice green leaf."

In Treetop is an acorn.  The acorn is a supporter.  The description of the acorn is "It's the largest acorn you've ever seen, hanging precariously from a branch far above the earth."

Instead of examining the acorn in treetop:
	try pushing the acorn.
	
Instead of pulling the acorn in treetop:
	try pushing the acorn.
	
Instead of pushing the acorn in treetop:
	now the cricket is bandaged;
	move acorn to Pleasant Hill;
	now the description of acorn is "It's the largest acorn you've ever seen.";
	say "You carefully reach down to touch it, and feel its rough texture -- but your bump was just the nudge it needed to fall from the tree.  You barely keep your balance as you watch it drop down.  The wind catches it as it falls, carrying it west in the direction of.... uh oh.  Oh dear.

This is, uhm... not good.  Not good at all.

You can't see what happens down below from way up here, but you hear a noisy kerfluffle.  There's a SMACK which sounds disturbingly like an acorn landing on the head of a surprised cricket.  Then you hear a series of loud and rather angry cricket chirps.  You wonder if the
grumpy cricket now has a headache.";

Instead of climbing Tall Tree in Treetop:
	try going down.

Carry out hinting in Treetop:
	say "[if cricket is unhappy]Have you taken a close look at that acorn?  I wonder what would happen if you would push it?[else]Maybe you should go down and check on the cricket.[end if]"
	
The leaf is scenery in Treetop.  "Yep, it's a leaf.  Did I mention that already?  Good.

Let's see.  Well, it's green.  Rather... leafy.  Blows around a lot.

You inspect it all over, and conclude: it's just an ordinary leaf."

Chapter - The School

Section - Schoolyard

North of Pleasant Hill is the Schoolyard.

Schoolyard is a room.  "You are just outside a large building.  Big letters on it spell out:

[quotation mark]WELCOME TO VERMONT ELEMENTARY[line break]
Conveniently located in Kansas[quotation mark]

A little path runs to the south.  There is an entrance to the north -- a big glass door."

The small etching is fixed in place in the schoolyard. The description of small etching is "It's a small engraving in the last letter 'a' of the sign.  It's just the right size for a small creature like you to read.  It says:

[quotation mark]A LETTER MANUFACTURING CO.[line break]
235 Go A Way[line break]
North Southeastern City, West Virginia[quotation mark]"

carry out hinting in Schoolyard:
	say "This might be a convenient time to GO NORTH.  Or not, it's up to you!
	
	Sometimes I like to examine things.  Things like etchings.  Who knows if it's helpful, but it might be fun."
	
Before going from Schoolyard to Lobby, say "You push hard on the big glass door, but it is just too big and heavy.  Fortunately, there's a gap underneath, so you can easily crawl under."

North of the Schoolyard is the Lobby.

Section - Lobby

The Lobby is a room.  "You're inside an big lobby.  The shiny floor feels cold and slippery underneath your bare feet.  There's no sun shining here, but plenty of light, and the air is cool and still.

On one of the walls there are two signs: one says INFORMATION, and the other says MENU.  It must be important, because a tall giant is standing there, glaring at the sign and making growling noises.

To the south is a glass door, still wet from being washed. [if the apple alert button is not stuck]Busy hallways extend east, west, and north.[else]The path north is now occupied only by apples, which look like you could crawl over.[end if]

There's a door to the southeast, next to a sign saying [quotation mark]RESTROOM.  CAUTION: Beware of exhaust.[quotation mark]  A door to the southwest has a sign near it saying [quotation mark]OFFICE[quotation mark]."

The menu is scenery in the lobby. "You wait patiently for the growling giant to give up staring at the sign and leave, then you walk carefully to the sign. The letters at the top are bigger than you!  Across the top, in big bold letters, it says -- or rather, it SAID:

[quotation mark]LUNCH MENU[quotation mark]

Someone with scrawly handwriting has crossed out [quotation mark]MENU[quotation mark], and written [quotation mark]CAUTION[quotation mark] above lunch.  So now, it reads, [quotation mark]CAUTION LUNCH.[quotation mark]

Next to each typed line under the caution lunch, someone has written a
comment, like so:

[quotation mark]LIVER AND RICE STEW  (barf)

STEAMED SPINACH WITH JELLO  (gag)

BOILED GIZZARDS  (ptooey)

WATERMELON IN CLAM SAUCE  (nasty)

LEFTOVER LETTUCE SALAD  (with roaches)

ICE CREAM WITH CRUSHED ERASERS (yuck)[quotation mark]

In the blank space at the bottom of the sign, someone wrote, in large
scrawly letters, [quotation mark]WE WANT PIZZA!!!![quotation mark]  The word [quotation mark]pizza[quotation mark] is underlined
seven and a half times."

The information sign is scenery in the lobby.  "The information sign is stuck to the wall, looking strong and sturdy and unmoving in just the way acorns don't.  The sign says:

INFORMATION

EAST HALLWAY: 1st Grade (Mrs. Crabapple), Library (Mr. Manybooks), and other stuff.

WEST HALLWAY: Art (Mrs. Splatterpaint), Band (Mr. Loudhorn), and more other stuff.

AHEAD TO THE NORTH: Kitchen (Mr. Ovenmitt), bus exit.

SOUTH: You came in from there.  Do you really need a sign telling you about it?

At the bottom, a small label reads:[line break]
Sog Eee Paper Co.[line break]
5182 N. Hewlett St.[line break]
Packard, AZ"

Instead of going east in the lobby, say "There are so many giants with backpacks walking around.  You are afraid of getting squished under one of them, and won't go that way."

Instead of going west in the lobby, say "There are so many giants with backpacks walking around.  You are afraid of getting squished under one of them, and won't go that way."

Instead of going north in the lobby when the apple alert button is not stuck, say "There are so many giants with backpacks walking around.  You are afraid of getting squished under one of them, and won't go that way.  You consider crawling across the ceiling, but there are hot lights there.  You'll have to think of something else."

carry out hinting in the lobby: 
	say "[if apple alert button is not stuck]Try to find something to do to get all the big people away from the path north.  You might find something useful in the rooms you can get to.[else]I guess now you can go north![end if]

Maybe you'd also like to read the menu or the information?"

Southeast of the lobby is the Restroom.

Southwest of the lobby is the Office.

North of the lobby is the Bus Plaza.

The apples is scenery.  "It's a big pile of delicious-looking apples.  Each apple has a sticker on it, which has little letters that say:

[quotation mark]This is an apple[line break]
You probably knew that[line break]
Life is so much better[line break]
When you know I'm not cattle[line break]

Labeled by the Not-So-Useful Labeling Company's Labels[line break]
19151 West South Ambiguous Ditch Northeast[line break]
Big Sign, CA[quotation mark]"


Section - Restroom

Before going from the lobby to the restroom, say "You crawl through some ventilation slots in the door.  It could probably use more ventilation than that, even."

The Restroom is a room.  "You are in the restroom.  You hear some loud rumbles coming from behind the stall door.  You dare not get too close.

The doorway to the lobby is to the northwest."

In the restroom is a paper towel scrap.  The description of the paper towel scrap is "It's a corner of a paper towel that was torn off.  [if apple alert button is dirty]It looks clean and dry.  It might be able to clean something small.[else]It is sticky and yucky as if it's been used to clean something.[end if]  Written in small print in the corner of the scrap is this faint text:

[quotation mark]Kinda Clean Toweling Co.[line break]
23 and a half North Muddy Way[line break]
Northern South Dakota[quotation mark]"

The mirror is fixed in place in the restroom.  The description of the mirror is "It's a very large mirror, extending all the way from the floor to the ceiling.  It works pretty much like every other mirror does, showing a reflection.  You strut in front of it, admiring your strong legs.  How many legs do you have?  You start to count.  One, two, eight, fifty-six, four...  You never were good at counting.  But you are sure you have just one head.

Attached to the far bottom of the mirror is a dirty label.  It reads:

MADE BY THE REFLECTIVE MIRRORING COMPANY OF FOGGY SHORE, LONDON[line break]
[quotation mark]We let you see yourself so we don't have to[quotation mark]"

The stall door is scenery in the restroom.  The description of the stall door is "The stall door is closed.  You're not sure why, but you think that's probably good.

You see a faded label on the door.  It says:

Sanitary Door Manufacturing and Distribution Corp., Ph. D., Inc., Esq., GmbH[line break]
819 Smelly Wall Dr.[line break]
Walla Walla, Washington"

Instead of opening the stall door in the restroom, say "You take 2 and a half steps towards the stall door, then go back.  You decide it might not be a good idea to open that door.  Partly because it wouldn't be very nice.  Also because you suspect it might be quite stinky."

carry out hinting in the restroom:
	say "[if the player has the paper towel scrap]You're carrying the paper towel scrap; good.  Try it out -- elsewhere.[else]Maybe you should TAKE TOWEL?[end if]"
	
Instead of smelling in the restroom, say "You wave your antennae about expectantly.  Ewww!  That's not a nice smell.  You stop your antennae-waving immediately!"

Section - Office

The Office is a room.  "You are in an office.  It has large windows facing the hallways.  Nobody else is here right now.

The doorway to the lobby is to the northeast."

Instead of smelling in the office, say "You wave around your antennae happily.  You detect the scent of paper.  Lots of paper."

carry out hinting in the office:
	say "[if apple alert button is not stuck]Is there something here you can climb or GO UP?[else]Maybe something has happened in the lobby?[end if]"
	
The desk is fixed in place in the office.  The description of the desk is "It's one of those things that holds paper and machinery.  The legs look like they'd be fun to climb. 

One of the legs has a small tag on it.  The tag reads:

Mfd. by:[line break]
Wobble E. Desk Co.[line break]
10537 Northeast West Street SE[line break]
Old York, NJ"

Instead of climbing the desk in the office:
	try going up.
	
Above the office is the Desktop.

Before going to the office:
	Now the desk is in the office.

Section - Desktop

The Desktop is a room.  "You are walking around on top of the desk.  This is fun, way up high!  The desktop is mostly clean."

Before going to the desktop:
	Now the desk is in the desktop.
	
Carry out hinting in the desktop:
	say "[if apple alert button is not stuck]Maybe an alert can get the people to move away from the hallway so you can walk there?[else]See if you can GO DOWN and get past the busy hallway now![end if]"

The Alert-O-Matic is fixed in place in the desktop.  The description of Alert-O-Matic is "It seems to be some sort of automatic alerting machine.  Maybe it plays messages on the speakers throughout the school?  There are buttons on it.

The fine print at the back of the Alert-O-Matic says:

Next-to-the-last National Alerting Co.[line break]
53 Click and Clack Drive[line break]
Tappet Bros. Square[line break]
Cambridge -- OUR FAIR CITY -- MA"

Instead of climbing the desk in the desktop:
	try going down.
	
Section - Fire Alert

On the Alert-O-Matic is a fire alert button.  The description of the fire alert button is "It's a small button on the Alert-O-Matic.  The label says [quotation mark]FIRE ALERT[quotation mark].  Very tiny text at the bottom of the label says:

Made by the Not On Fire Any More Corp.[line break]
5853 N. O'Leary Dairy Drive[line break]
Chicago, Ill"

Instead of pushing the fire alert button, say "You climb up on the fire alert button.  You push, but nothing happens until you try jumping on it.

It emits a loud click and a soft clack.  Then you hear all the speakers in the school play a message:

[quotation mark]WEEEEooooo.  WEEEEoooo.  WEEEEoooo.  FIRE alert.  FIRE ALERT! FIRE ALERT![quotation mark]

You see lots of people running around looking puzzled.  Two of them seem to be in an argument.

[quotation mark]Oh no.  Not AGAIN.[quotation mark]

[quotation mark]Yes, Skeezix, it's another fire alert.[quotation mark]

[quotation mark]What do you think it MEANS?[quotation mark]

[quotation mark]Probably that there's a fire and that we should run away.[quotation mark]

[quotation mark]No no no!  You're so wrong, Meesix.  I'm sure it means that the fire is out and we should build a new fire![quotation mark]

[quotation mark]Build a fire?  In a school?  You're crazy![quotation mark]

[quotation mark]Well I just built one.  I guess it's gone out and I have to build another one.[quotation mark]

[quotation mark]Why would you have to build another one?[quotation mark]

[quotation mark]To keep the pottery in the art oven hot.[quotation mark]

[quotation mark]Why wouldn't Mrs. Splatterpaint do that herself?[quotation mark]

[quotation mark]Well, I don't know.  You heard the alert!  Time to build a fire![quotation mark]

Another person walks by.  

[quotation mark]You know what I think the alert means?[quotation mark]

[quotation mark]No, Darryl, what does it mean?[quotation mark]

[quotation mark]That I'm going to get the first lunch because you two are so busy arguing about alerts.[quotation mark]"

Section - Christmas Alert

On the Alert-O-Matic is a Christmas alert button.  The Christmas alert button can be working or broken.  The Christmas alert button is working.  The description of the Christmas alert button is "[if the Christmas alert button is working]It's a small button on the Alert-O-Matic.  It looks festive, decorated in red and green.  The label says:

CHRISTMAS ALERT

Mfg. by Alerts of Santa Day[line break]
25 N. Santa Way[line break]
Santa Fe[else]It's a small button on the Alert-O-Matic.  The label is scorched, and the button is broken.  It won't work anymore.[end if]"

Instead of pushing the Christmas alert button when the Christmas alert button is working:
	Now the Christmas alert button is broken;
	say "You climb onto the Christmas alert button.  You jump on it to push it down.
	
	It emits a soft click and a loud clack.  Then you hear all the speakers in the school start to ring out with an alerting song:
	
	[quotation mark]Dashing through the snow[line break]
	in a one-horse open sleigh,[line break]
	O'er the school we go,[line break]
	Alerting all the way![quotation mark]
	
	You see excited people running around dancing.  The alert continues:
	
	[quotation mark]Oh, Christmas alert!  Christmas alert![line break]
	Alerting all the way![line break]
	Oh what fun it is to have[line break]
	A Christmas alert to-day![quotation mark]
	
	There are many alerting verses to the song.  You doze off for a while, only waking up for the great snow-crises of verse 23, in which everyone is afraid the Christmas alert is stuck in a snowbank.  Fortunately, that's resolved in verse 32, in which everyone realizes there is no snow due to it being a warm day.
	
	Finally, at around verse 73, in which people realize the song has gone on so long that is now approximately April, the Christmas alert button starts to smoke.  It sizzles and fizzles, and then with a loud POP, the Christmas alert is over."
	
Instead of pushing the Christmas alert button when the Christmas alert button is broken:
	Say "That button is broken due to overuse and it doesn't do anything anymore.".

Section - AlertAlert

On the Alert-O-Matic is an AlertAlert button. The description of the AlertAlert button is "The AlertAlert button is a small, square button on the Alert-O-Matic.  The label on it says:

ALERTalert[line break]
Made by the Redundancy in Alerting Foundation[line break]
Made by the Redundancy in Alerting Foundation[line break]
Department of Redundancy Dept.[line break]
5351 Bachwards Logic Dr.[line break]
University of Southern North Dakota at Hoople"

Instead of pushing the alertalert button, say "You climb on the AlertAlert button.  You jump on it to push it down.  It makes a squishy click and a concerning clack.  Then you hear the school's speaker system playing the alert:

[quotation mark]ALERT alert.  ALERT alert.  ALERT alert.  alert ALERT?[quotation mark]

Some giants are arguing about it outside the office window.

[quotation mark]What's an alert ALERT?[quotation mark]

[quotation mark]No, it's an ALERT alert.[quotation mark]

[quotation mark]Well Skeezix, what's an ALERT alert then?[quotation mark]

[quotation mark]It's simple, Meesix.  It's an alert saying you should be alert for alerts![quotation mark]

[quotation mark]What kind of alerts?[quotation mark]

[quotation mark]The ALERT alerts, of course![quotation mark]

[quotation mark]That doesn't make any sense.[quotation mark]

[quotation mark]Yes it does![quotation mark]

[quotation mark]Why should there be an alert for ALERT alerts?  All it would be saying is to be alert for more ALERT alerts.[quotation mark]

[quotation mark]Yes, and then you would be very alert for useless alerts.  It's great![quotation mark]"

Section - Apple Alert

On the Alert-O-Matic is an apple alert button.  The apple alert button can be dirty, clean, or stuck.  The apple alert button is dirty.  The description of the apple alert button is "The apple alert button is a friendly-looking button in the Alert-O-Matic.  [if apple alert button is dirty]It is all sticky because it looks like someone spilled some apple juice on it and never bothered to clean it up.[end if][if apple alert button is stuck]It's stuck and can't be used again.[end if]

The label on the apple alert button says:

APPLE ALERT[line break]
Jon E. App LeSeed Alert Company[line break]
9980 S. Ider Ave.[line break]
Upper East Side of the Western Lower Peninsula[line break]
General vicinity of Michigan"

Instead of pushing the apple alert button when the apple alert button is dirty, say "You climb up to the apple alert button, and are about to jump on it, when you realize it is really... sticky.  It looks like someone spilled apple juice on it.  You can't jump on that, because then your feet might stick to the button.  Maybe you can find something to clean it with?[if the player has the paper towel scrap]Maybe you can rub it clean with the paper towel scrap you're carrying around like a weirdo.[else]You figure you'll need to find something to clean it with.[end if]"

Instead of pushing the apple alert button when the apple alert button is stuck, say "The apple alert button is stuck.  Maybe the spilled apple juice got inside it.  It can't be pushed again."

Instead of pushing the apple alert button when the apple alert button is clean:
	Now the apple alert button is stuck;
	say "You climb up on the apple alert button.  You jump on it and it comes to life.  You hear the school speakers say:
	
	[quotation mark]EMERGENCY!  Apple alert!  Apple alert!  APPLE ALERT!  C'mon people, it's an APPLE ALERT![quotation mark]
	
	You watch what happens out the window.  There's an argument:
	
	[quotation mark]What's an apple alert?  Apples aren't scary.[quotation mark]
	
	[quotation mark]They are if there's a mountain of them taller than you.[quotation mark]
	
	[quotation mark]That would never happen![quotation mark]
	
	Just then, you hear the [quotation mark]BEEP BEEP BEEP[quotation mark] of a delivery truck backing up to the wrong door.  The delivery truck has a big red apple painted on the side, alongside a sign saying [quotation mark]Apple Deliveries, Inc.  Mis-delivering your apples since 1953.[quotation mark]
	
	You watch as dozens -- or maybe thousands (you were never good at counting) -- of apples dump out of the truck.  Nobody can walk past the north hall now because there's an apple pile in it.
	
	You hear, [quotation mark]Oh, so THAT'S an apple alert![quotation mark]
	
	You notice the apple alert button is stuck down.  Maybe that old apple juice ran inside it or something."

Instead of rubbing the apple alert button when the player has the paper towel scrap and the apple alert button is dirty:
	Now the apple alert button is clean;
	Now the apples is in the lobby;
	say "You rub the apple alert button with the paper towel scrap.  You get the stickiness off and it looks safe to touch now."
	
Instead of rubbing the apple alert button when the player has the paper towel scrap and the apple alert button is not dirty:
	say "You already cleaned the apple alert button with the paper towel scrap.  Now that the towel is dirty, you can't really clean with it anymore."
	
Instead of rubbing the apple alert button when the player does not have the paper towel scrap:
	say "You have nothing to clean the apple alert button with.  Maybe you can find something smallish and towelish somewhere?"
	
	[
Instead of cleaning in the the apple alert button:
	Try rubbing the apple alert button.
	]

Section - Pizza Alert

On the Alert-O-Matic is a pizza alert button.  The description of the pizza alert button is "It's a smallish and roundish button, sitting on the topish of the Alert-O-Matic.  The label on the button says:

PIZZA ALERT[line break]
Made by the Pep O. Roni Company[line break]
53 and a half Slices Drive[line break]
Venice"

Instead of pushing the pizza alert button, say "You climb up on the pizza alert button.  You push, but nothing happens until you try jumping on it.

It emits a comforting click and a confused clack.  Then you hear all the speakers in the school play a message:

[quotation mark]Attention!  This is a pizza alert.  PIZZA ALERT!  PIZZA ALERT![quotation mark]

You hear shouts of [quotation mark]YAY![quotation mark] echo through the school.  Lots of people with backpacks fill the halls, running in the direction of the kitchen.  A surprised-looking Mr. Ovenmitt looks from the window and hears the chants of [quotation mark]WE WANT PIZZA!  WE WANT PIZZA![quotation mark]  He responds, [quotation mark]No pizza today!  Ice cream with crushed erasers for you![quotation mark]

Disappointed backpacks -- and the people waring them -- sadly return to what they were doing."

Chapter - The School Bus

Section - Bus Plaza

Before going from the lobby to the bus plaza when the apple alert button is stuck, say "You crawl over the pile of apples.  Although each apple is bigger than you, you laugh because it's easier for a small creature like you to climb over the pile than it is for the giants.  Silly giants!"

The Bus Plaza is a room.  "You're just outside the school on the north side.

To the south is the school."

The school bus is a vehicle in the bus plaza.  "A yellow school bus is waiting here."

The description of the school bus is "[if the player is not in the school bus]You look at the school bus.  You know it's a school bus because it is yellow, you first saw it at a school, and it has a sign saying [quotation mark]SCHOOL BUS[quotation mark] on it.  It also has a sign saying [quotation mark]Diesel fuel only[quotation mark], but that sign is rather less helpful.[else]You're inside the school bus!  You see lots of shoelaces.  They seem to be connected to shoes.  You imagine there are feet in those shoes, but it's hard to tell for sure.  You've found a comfy, soft, and worm corner where there are some leaves and dust piled up."

Carry out hinting in the bus plaza:
	Say "Perhaps you should try to ENTER BUS?"
	
Instead of going up in the bus plaza:
	Try entering the school bus.
	
Instead of climbing in the bus plaza:
	Try entering the school bus.
	
After entering the school bus in the bus plaza:
	Say "You climb up one of the black tires, then crawl under the gap at the door.  You did it -- you're in the bus!
	
	The bus gets louder and bumpy.  It is driving off with you inside it.  After a little while, it comes to a stop.  You think this might be a convenient time to exit.";
	move the bus to the Anthill Entrance.	

Section - Anthill Entrance

The Anthill Entrance is a room.  "You're standing on a little hill.  An anthill, you suppose.  Your feet feel the soft hill beneath them while your antennae wave gently in the wind.  This is a familiar place.  In the middle of the anthill is a little passageway heading down."

After exiting in the anthill entrance:
	Say "You happily jump down from the school bus -- WHEEEE!  The school bus drives off to take others home.";
	Now the bus is in the bus plaza;
	move the player to the anthill entrance. [to display the description]

Carry out hinting in the anthill entrance when the player is in the school bus:
	Say "This might be a good time to EXIT".
	
Carry out hinting in the anthill entrance when the player is not in the school bus:
	Say "Perhaps you should GO DOWN?"
	
Below the anthill entrance is the Dining Room.

Section - Dining Room

The Dining Room is a room.  "You are in a warm and cheerful anthill, right in the middle of the dining room.  Your mom spots you and says, [quotation mark]You're home, and just in time for supper![quotation mark]  You get a little hug.

Your dad walks in, gives you a big hug, and says, [quotation mark]We have your favorite meal today: picnic food!  There was a great picnic up there today.  We got some bits of ham, beans, and even peach pie.  They didn't even notice we got any![quotation mark]

You glance around the room.  On the wall is the your favorite painting of the last supper (it's new; that was yesterday's supper) featuring all 137 of your closest relatives around the table.  There you are towards the end, alongside all the other ants.  You smile.

Mom says, [quotation mark]And after dinner, go check the dining room cabinet.  We put a few special treats in there for you![quotation mark]"

After looking in the dining room:
	Say "Ahhh.... It is so good to be home.";
	End the story saying "You have won the game!"
	 