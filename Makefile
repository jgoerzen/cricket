TESTS := $(wildcard jgoerzen-tests/*.t)

.PHONY: all
all: test

.PHONY: %.t
%.t: TEST
	./jgoerzen-tools/texpect $@ > $@.testlog

.PHONY: test
test: $(TESTS)

TEST:

